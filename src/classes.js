class Hero {
    constructor(name, health, power) {
        this.name = name
        this.health = health
        this.power = power
    }

    hit(hero) {
        if (hero.health > 0) {

            hero.health -= this.power
        }
        if (hero.health < 0) {
            this.health = 0
        }
    }
}

class MarvelHero extends Hero {
    constructor(name, health, power, healPower) {
        super(name, health, power);
        this.healPower = healPower

    }

    heal() {
        this.health += this.healPower
    }

}

class DCHero extends Hero {
    superHit(hero) {
        if (this.health < 0) {

            hero.health -= this.power * 2
        }
        if (this.health < 0) {
            this.health = 0
        }
    }
}

module.exports = {DCHero, MarvelHero}
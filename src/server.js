const {DCHero, MarvelHero} = require("./classes")
const express = require('express');
const app = express();
const moment = require("moment");
var bodyParser = require('body-parser')


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static('dist'));

app.get('/time', function (req, res) {
    res.send(moment().format('DD-MM-YYYY hh:mm:ss'))
})

let users = [{name: 'John'}];
let msgs = [];
let msgId = 0;

app.get('/users', (req, res) => {
    // let jsonData = JSON.stringify(users)
    res.send(users)
})

app.get('/messages', ((req, res) => {
    res.send(msgs)
}))

app.post('/login', ((req, res) => {
    console.log('body', req.body)
    let user = req.body;
    users.push(user)
    res.send(users)
}))

app.post('/send-message', (req, res) => {
    let msg = req.body;
    sendMessage(msg)

    res.send({})
})

app.listen(3000, () => {
    console.log('Server started!')
})

function sendMessage(msg) {
    msg.id = msgId++;
    msgs.push(msg)
}

let h1 = new MarvelHero("Spider", 250, 10, 5)
let h2 = new MarvelHero("Tor", 220, 15, 15)
let h3 = new MarvelHero("Hulk", 240, 10, 5)

let d1 = new DCHero("Batman", 250, 20)
let d2 = new DCHero("Capitan", 220, 20)
let d3 = new DCHero("SuperMan", 240, 25)

const heroesA = [h1, h2, h3]
const heroesB = [d1, d2, d3]
const heroes = [...heroesA, ...heroesB]

app.get('/heroes', (req, res) => {
    res.send(heroes)
})

app.get('/hero/:index', (req, res) => {
    console.log('req', req.params.index)
    let hero = heroes[req.params.index]
    res.send(hero)
})

app.get('/fight', (req, res) => {
    if (!int)
        startFight();
})

let roundId = 0;
let int;
function startFight() {
    int = setInterval(round, 1000)
}

function stopFight() {
    clearInterval(int)
    int = null;
}

function round() {
    roundId++;

    for (let heroB of heroesB) {
        for (let heroA of heroesA) {
            heroA.hit(heroB)
            heroA.heal()
            if (heroA.health < 0) {
                heroA.health = 0
            }

            heroB.hit(heroA)
            heroB.superHit(heroA)
            if (heroB.health < 0) {
                heroB.health = 0
            }
        }
    }

    console.log('round id', roundId);

    let heroInfo = [
        ...heroesA.map(({name, health}) => `${name}: ${health}`),
        ...heroesB.map(({name, health}) => `${name}: ${health}`)
    ].join(', ')

    sendMessage({
        name: 'System',
        msg: `round ${roundId}: ${heroInfo}`
    })

    if (roundId > 10) {
        stopFight()
    }


}